import { https } from "./config";

export const adminService = {
  getUserList: () => {
    return https.get(`/api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=GP00`);
  },
  getMovieList: () => {
    return https.get("/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP04");
  },
};
