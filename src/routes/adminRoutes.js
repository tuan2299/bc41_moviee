import AdminLayout from "../Layout/AdminLayout";
import AdminMoviesPage from "../Page/AdminMoviesPage/AdminMoviesPage";
import AdminUsersPage from "../Page/AdminUsersPage/AdminUsersPage";
import LoginPage from "../Page/LoginPage/LoginPage";

export const adminRoutes = [
  {
    url: "/login",
    component: <LoginPage />,
  },
  {
    url: "/admin-users",
    component: <AdminLayout Component={AdminUsersPage} />,
  },
  {
    url: "/",
    component: <AdminLayout Component={AdminUsersPage} />,
  },
  {
    url: "/admin-movies",
    component: <AdminLayout Component={AdminMoviesPage} />,
  },
];
