import { Table } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import Spinner from "../../Components/Spinner/Spinner";
import { adminService } from "../../service/adminService";
import { setLoadingOff, setLoadingOn } from "../../toolkit/spinnerSlice";
import { headerColums } from "./utils";

export default function AdminUsersPage() {
  const [userList, setUserList] = useState([]);
  let dispatch = useDispatch();
  useEffect(() => {
    dispatch(setLoadingOn());
    adminService
      .getUserList()
      .then((res) => {
        dispatch(setLoadingOff());
        console.log(res);
        setUserList(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  console.log(userList.length);
  return (
    <div>
      <Table columns={headerColums} dataSource={userList} />
    </div>
  );
}

//react spinner

//git fetch
//git checkout admin
