import { Tag } from "antd";

export const headerColums = [
  {
    title: "Tài khoản",
    dataIndex: "taiKhoan",
    key: "taiKhoan",
  },
  {
    title: "Họ Tên",
    dataIndex: "hoTen",
    key: "hoTen",
  },
  {
    title: "Email",
    dataIndex: "email",
    key: "email",
  },
  {
    title: "Số điện thoại",
    dataIndex: "soDT",
    key: "soDT",
  },
  {
    title: "Mật Khẩu",
    dataIndex: "matKhau",
    key: "matKhau",
  },
  {
    title: "Loại người dùng",
    dataIndex: "maLoaiNguoiDung",
    key: "maLoaiNguoiDung",
    render: (loai) => {
      if (loai == "KhachHang") {
        return (
          <Tag className="font-medium" color="green">
            Khách Hàng
          </Tag>
        );
      } else {
        return (
          <Tag className="font-medium" color="red">
            Quản Trị
          </Tag>
        );
      }
    },
  },
];

// {
//     "taiKhoan": "111111",
//     "hoTen": "1234",
//     "email": "cuphong2003hk@gmail.com",
//     "soDT": "213412343214",
//     "matKhau": "12345",
//     "maLoaiNguoiDung": "KhachHang"
// }
