import React from "react";

export default function NotFoundPage() {
  return (
    <div className="flex justify-center items-center">
      <h2 className="text-2xl font-black text-red-500 animate-spin transform translate-y-10">
        NotFoundPage
      </h2>
    </div>
  );
}
