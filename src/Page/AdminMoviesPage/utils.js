import { Tag } from "antd";

export const headerColums = [
  {
    title: "Mã Phim",
    dataIndex: "maPhim",
    key: "maPhim",
  },
  {
    title: "Tên Phim",
    dataIndex: "tenPhim",
    key: "tenPhim",
  },
  {
    title: "Mô Tả",
    dataIndex: "moTa",
    key: "moTa",
    render: (moTa) => {
      return <p className="w-96">{moTa}</p>;
    },
  },
  {
    title: "Hình Ảnh",
    dataIndex: "hinhAnh",
    key: "hinhAnh",
    render: (hinhAnh) => {
      return <img src={hinhAnh} className="w-20" />;
    },
  },
  {
    title: "Đánh Giá",
    dataIndex: "danhGia",
    key: "danhGia",
    render: (danhGia) => {
      if (danhGia > 5) {
        return (
          <Tag className="font-medium" color="green">
            {danhGia}
          </Tag>
        );
      } else {
        return (
          <Tag className="font-medium" color="red">
            {danhGia}
          </Tag>
        );
      }
    },
  },
  {
    title: "Hành Động",
    dataIndex: "action",
    key: "action",
  },
];

// {
//     "taiKhoan": "111111",
//     "hoTen": "1234",
//     "email": "cuphong2003hk@gmail.com",
//     "soDT": "213412343214",
//     "matKhau": "12345",
//     "maLoaiNguoiDung": "KhachHang"
// }

// {
//   "maPhim": 1422,
//   "tenPhim": "Fantastic Four 2",
//   "biDanh": "fantastic-four-2",
//   "trailer": "https://www.youtube.com/embed/AAgnQdiZFsQ",
//   "hinhAnh": "https://movienew.cybersoft.edu.vn/hinhanh/fantasticfour.jpg",
//   "moTa": "Four young outsiders teleport to an alternate and dangerous universe which alters their physical form in shocking ways. The four must learn to harness their new abilities and work together to save Earth from a former friend turned enemy.",
//   "maNhom": "GP04",
//   "ngayKhoiChieu": "2022-11-20T00:00:00",
//   "danhGia": 7,
//   "hot": true,
//   "dangChieu": true,
//   "sapChieu": true
// }
