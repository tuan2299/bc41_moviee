import { Button, Table } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import Spinner from "../../Components/Spinner/Spinner";
import { adminService } from "../../service/adminService";
import { setLoadingOff, setLoadingOn } from "../../toolkit/spinnerSlice";
import { headerColums } from "./utils";

export default function AdminMoviesPage() {
  const [movieList, setMovieList] = useState([]);
  let dispatch = useDispatch();
  useEffect(() => {
    dispatch(setLoadingOn());
    adminService
      .getMovieList()
      .then((res) => {
        dispatch(setLoadingOff());
        let list = res.data.content.map((movie) => {
          return {
            ...movie,
            action: (
              <Button
                type="primary
          "
                danger>
                Delete
              </Button>
            ),
          };
          // table antd
        });
        console.log(res);
        setMovieList(list);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  console.log(movieList.length);
  return (
    <div>
      <Table columns={headerColums} dataSource={movieList} />
    </div>
  );
}

//react spinner

//git fetch
//git checkout admin
