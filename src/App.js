import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from "./Page/HomePage/HomePage";
import LoginPage from "./Page/LoginPage/LoginPage";
import DetailPage from "./Page/DetailPage/DetailPage";
import Layout from "./Layout/Layout";
import BookingPage from "./Page/BookingPage/BookingPage";
import NotFoundPage from "./Page/NotFoundPage/NotFoundPage";
import { adminRoutes } from "./routes/adminRoutes";
import AdminLayout from "./Layout/AdminLayout";
import Spinner from "./Components/Spinner/Spinner";

function App() {
  return (
    <div>
      <Spinner />
      <BrowserRouter>
        <Routes>
          {adminRoutes.map(({ url, component }) => {
            return <Route path={url} element={component} />;
          })}
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
