import React from "react";
import { useSelector } from "react-redux";
import { BeatLoader } from "react-spinners";
import PropagateLoader from "react-spinners/PropagateLoader";
import RingLoader from "react-spinners/RingLoader";

export default function Spinner() {
  let { isLoading } = useSelector((state) => state.spinnerSlice);
  return isLoading ? (
    <div
      style={{ background: "#edf2f4" }}
      className="fixed top-0 left-0 z-50 flex items-center justify-center w-screen h-screen">
      <RingLoader color="#2b2d42" size={150} speedMultiplier={1.5} />
    </div>
  ) : (
    <div></div>
  );
}
