import { createSlice } from "@reduxjs/toolkit";
import { localUserServ } from "../service/localService";

const initialState = {
  userInfo: localUserServ.get(),
};

const userSlice = createSlice({
  name: "userSlice",
  initialState,
  reducers: {
    setLoginUser: (state, action) => {
      state.userInfo = action.payload;
    },
  },
});

export const { setLoginUser } = userSlice.actions;

export default userSlice.reducer;

//rxslice
